import 'package:flutter/material.dart';
import 'package:portfolio/widgets/body.dart';
import 'package:portfolio/widgets/button_row.dart';
import 'package:portfolio/widgets/contact_button.dart';
import 'package:url_launcher/url_launcher.dart';

class Portfolio extends StatelessWidget {
  const Portfolio({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        backgroundColor: Colors.white,
        title: Row(
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 9.0),
              child: CircleAvatar(
                radius: 25,
                backgroundColor: Colors.black,
                foregroundImage: NetworkImage(
                    'https://pbs.twimg.com/profile_images/1461219695793737730/VaBWwkJT_400x400.jpg'
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                  'Alexander',
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 20,
                    fontWeight: FontWeight.w500,
                    fontStyle: FontStyle.italic,
                  )
              ),
            )
          ],
        ),
        actions: [
          ContactButton(
            buttonText: 'Contact me!',
            icon: Icon(Icons.send_sharp),
            onPressed: () => launch('mailto:spartagen@gmail.com'),
          ),
        ],
      ),
      body: Stack(
        children: [
          Body(),
          Align(
            alignment: Alignment.bottomLeft,
            child: Padding(
              padding: const EdgeInsets.all(98.0),
              child: SizedBox(
                height: 59,
                child: ButtonRow()
              ),
            )
          ),
        ],
      ),
    );
  }
}


