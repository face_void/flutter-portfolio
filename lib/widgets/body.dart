import 'package:flutter/material.dart';
import 'package:portfolio/widgets/contact_button.dart';
import 'package:url_launcher/url_launcher.dart';

class Body extends StatelessWidget {
  const Body({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final List<Map<String, String>> projectsList = [
      {
        'title': 'Building a Cat',
        'subtitle': 'Great client',
        'image': 'https://picsum.photos/id/100/400/300',
      },
      {
        'title': 'Connekto',
        'subtitle': 'A Flutter app for nerds',
        'image': 'https://picsum.photos/id/100/400/300',
      },
      {
        'title': 'Been There',
        'subtitle': 'Save places you\'ve visited',
        'image': 'https://picsum.photos/id/100/400/300',
      },
      {
        'title': 'Bengo',
        'subtitle': 'Flutter email app',
        'image': 'https://picsum.photos/id/100/400/300',
      },
    ];

    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        Expanded(
          flex: 1,
          child: Container(
            color: Colors.white,
            child: Column(
              children: [
                Expanded(
                  child: Stack(
                    alignment: Alignment.center,
                    children: [
                      Opacity(
                        opacity: 0.5,
                        child: Image.asset('headshot.png'),
                      ),
                      Align(
                        alignment: Alignment.topCenter,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              'Меня зовут Александр\nЯ программист на PHP\nи Flutter',
                              style: TextStyle(
                                  fontSize: 44.5,
                                  color: Colors.blueGrey
                              ),
                            ),
                            Align(
                              alignment: Alignment.centerLeft,
                              child: Padding(
                                padding: EdgeInsets.symmetric(horizontal: 230.0, vertical: 60.0),
                                child: ContactButton(
                                  buttonText: 'Drop me a line',
                                  icon: Icon(Icons.mail_outline),
                                  onPressed: () => launch('mailto:spartagen@gmail.com'),
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
        Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                height: 160,
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  'Мои проекты',
                  style: TextStyle(
                    color: Colors.black54,
                    fontWeight: FontWeight.w600,
                    fontSize: 23,
                  ),
                ),
              ),
              Expanded(
                child: ListView.builder(
                  itemCount: projectsList.length,
                  itemBuilder: (context, index) {
                    return Container(
                      child: Column(
                        children: [
                          Card(
                            elevation: 3,
                            child: Column(
                              children: [
                                ListTile(
                                  leading: Icon(Icons.work),
                                  title: Text(projectsList[index]['title'] ?? ''),
                                  subtitle: Text(projectsList[index]['subtitle'] ?? ''),
                                ),
                                Container(
                                  alignment: Alignment.center,
                                  child: Padding(
                                    padding: const EdgeInsets.only(bottom: 40),
                                    child: Image.network(projectsList[index]['image'] ?? ''),
                                  ),
                                )
                              ],
                            ),
                          )
                        ],
                      ),
                    );
                  },
                ),
              ),
            ],
          ),
        ),
        SizedBox(width: 100,)
      ],
    );
  }
}
